package spring.flights;

import org.junit.Test;
import spring.flights.Domain.Flight;
import spring.flights.Service.FlightService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FlightServiceTest {
    private FlightService flightService = new FlightService();

    @Test
    public void getSingleFlightTest() {
        flightService = new FlightService();
        Flight flight = flightService.getSingleFlight();
        assertEquals(200, flight.getTickets().get(0).getPrice());
    }

    @Test
    public void getFlightsTest() {
        flightService = new FlightService();
        List<Flight> flightList = flightService.getFlights();
        assertEquals(2, flightList.size());
    }
}
