package spring.flights.Controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import spring.flights.Domain.Flight;
import spring.flights.Service.FlightService;

import java.util.List;

@RestController
public class FlightController {

    private FlightService flightService = new FlightService();

    @GetMapping("/flights/flight")
    public Flight getFlight() {
        return flightService.getSingleFlight();
    }

    @GetMapping("/flights")
    public List<Flight> getFlights() {
        return flightService.getFlights();
    }
}
