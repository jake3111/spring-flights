package spring.flights.Service;

import spring.flights.Domain.Flight;
import spring.flights.Domain.Passenger;
import spring.flights.Domain.Ticket;

import java.util.Date;
import java.util.List;
import java.util.Stack;

public class FlightService {

    public Flight getSingleFlight(){
        Passenger passenger = new Passenger("Jake", "Davis");
        Ticket ticket = new Ticket(passenger, 200);
        List<Ticket> ticketList = new Stack<>();
        ticketList.add(ticket);
        return new Flight(new Date(), ticketList);
    }

    public List<Flight> getFlights(){
        List<Flight> flightList = new Stack<>();
        //The first flight
        Passenger passenger = new Passenger("Jake", "Davis");
        Ticket ticket = new Ticket(passenger, 200);
        List<Ticket> ticketList = new Stack<>();
        ticketList.add(ticket);
        flightList.add(new Flight(new Date(), ticketList));

        //The second flight
        Passenger passenger2 = new Passenger("Chris", "Davis");
        Ticket ticket2 = new Ticket(passenger2, 250);
        List<Ticket> ticketList2 = new Stack<>();
        ticketList2.add(ticket2);
        flightList.add(new Flight(new Date(), ticketList2));

        return flightList;
    }
}
