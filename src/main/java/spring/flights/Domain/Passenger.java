package spring.flights.Domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Passenger {

    private String firstName;
    private String lastName;

    public Passenger(String firstName, String lastName){
        this.firstName = firstName;
        this.lastName = lastName;
    }

    @JsonProperty("FirstName")
    public String getFirstName() {
        return firstName;
    }

    @JsonProperty("LastName")
    public String getLastName() {
        return lastName;
    }
}
