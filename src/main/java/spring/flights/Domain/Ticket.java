package spring.flights.Domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Ticket {
    private Passenger passenger;
    private int price;

    public Ticket(Passenger passenger, int price){
        this.passenger = passenger;
        this.price = price;
    }

    @JsonProperty("Passenger")
    public Passenger getPassenger() {
        return passenger;
    }

    @JsonProperty("Price")
    public int getPrice() {
        return price;
    }
}
