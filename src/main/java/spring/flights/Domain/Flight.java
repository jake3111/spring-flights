package spring.flights.Domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;
import java.util.List;

public class Flight {

    private Date departs;
    private List<Ticket> tickets;

    public Flight(Date departs, List<Ticket> tickets){
        this.departs = departs;
        this.tickets = tickets;
    }

    @JsonProperty("Departs")
    public Date getDeparts() {
        return departs;
    }

    @JsonProperty("Tickets")
    public List<Ticket> getTickets() {
        return tickets;
    }
}
